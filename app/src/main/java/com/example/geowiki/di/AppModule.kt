package com.example.geowiki.di

import com.example.geowiki.data.repositories.*
import com.example.geowiki.data.source.remote.RoutesRemoteDataSource
import com.example.geowiki.data.source.remote.WikiRemoteDataSource
import com.example.geowiki.network.*
import com.example.geowiki.ui.MapViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val appModule = module {

    // Provide singleton of Wiki Retrofit API service
    factory (named("wikiOkHttp")){ provideWikiOkHttpClient() }
    single (named("wikiRetrofit")){ provideWikiRetrofit(get(named("wikiOkHttp"))) }
    single (named("wikiApi")){ provideGeowikiApi(get(named("wikiRetrofit"))) }
    // Provide singleton of Digitransit Retrofit API service
    factory (named("routesOkHttp")){ provideTransitOkHttpClient() }
    single (named("routesRetrofit")){ provideTransitRetrofit(get(named("routesOkHttp"))) }
    single (named("routesApi")){ provideDigitransitApi(get(named("routesRetrofit"))) }

    // Data sources
    factory { WikiRemoteDataSource(get(named("wikiApi"))) }
    factory { RoutesRemoteDataSource(get(named("routesApi"))) }

    // Provide Repositories
    single<WikiRepository> { WikiRepositoryImpl(get()) }
    single<RoutesRepo> { RoutesRepoImpl(get()) }
    single { LocationRepository() }

    // Provide ViewModels
    viewModel { MapViewModel(get(), get(), get()) }

}