package com.example.geowiki.utils

const val REQUEST_CHECK_SETTINGS = 11111
const val MAP_ZOOM_LEVEL_DEFAULT = 14f
const val LOCATION_REQUEST_INTERVAL: Long = 1000
const val USER_LOCATION_MARKER = 999999999999