package com.example.geowiki.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.Drawable
import androidx.annotation.ColorInt
import androidx.core.graphics.drawable.DrawableCompat
import com.example.geowiki.data.responsemodels.WikiLocation
import com.example.geowiki.data.responsemodels.WikiRoute
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.maps.android.PolyUtil


class MapUtils {

    companion object {

        fun convertPxToDp(context: Context, px: Float): Float {
            return px / context.resources.displayMetrics.density;
        }

        fun convertDpToPx(context: Context, dp: Float): Float {
            return dp * context.resources.displayMetrics.density
        }

        fun moveMapToPolylineBounds(polyline: ArrayList<LatLng>?, map: GoogleMap) {
            polyline?.let { list ->
                val builder = LatLngBounds.Builder()
                list.forEach {
                    builder.include(it)
                }
                val bounds = builder.build()
                val padding = 200
                val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
                map.moveCamera(cu)
            }
        }

        fun moveMapToLatLng(latLng: LatLng, map: GoogleMap, zoom: Float) {
            val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom)
            map.moveCamera(cameraUpdate)
        }

        fun animateMapToLatLng(latLng:LatLng,map: GoogleMap, zoom: Float) {
            val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom)
            map.animateCamera(cameraUpdate)
        }

        fun setMapPadding(dp: Float, context: Context?,  map: GoogleMap): Int? {
            context?.let {
                var pxOffset = (Math.round(convertDpToPx(it, dp)))
                map.setPadding(0, 0, 0, pxOffset)
                return pxOffset
            }
            return null
        }

        fun getLatLngFromWikiLocation(location: WikiLocation?): LatLng? {
            return location?.let {
                if (it.lat != null && it.lon != null) {
                    LatLng(it.lat!!, it.lon!!)
                } else {
                    null
                }
            }
        }

        fun getPolylineFromWikiRoutes(routes: List<WikiRoute>?): ArrayList<LatLng>? {
            routes?.let {
                var polyline = ArrayList<LatLng>()
                routes.forEach { wikiRoute ->
                    try {
                        if (!wikiRoute.legGeometry.isNullOrEmpty()) {
                            var route = wikiRoute.legGeometry
                            var points: String = ""
                            route!!.forEach { (key, value) ->
                                points = value
                            }
                            polyline.addAll(PolyUtil.decode(points))
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                return polyline
            }
            return null
        }

        fun addMarkerWithVectorIcon(
            map: GoogleMap, drawableIcon: Drawable, position: LatLng, color: Int, tag: Int = 0
        ): Marker {
            val marker = map.addMarker(
                MarkerOptions()
                    .position(position)
                    .icon(vectorToBitmap(drawableIcon, Color.parseColor("#6C33A8")))
            )
            marker.tag = tag
            return marker
        }

        private fun vectorToBitmap(vectorDrawable: Drawable, @ColorInt color: Int): BitmapDescriptor {
            val bitmap = Bitmap.createBitmap(
                vectorDrawable.intrinsicWidth,
                vectorDrawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmap)
            vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
            DrawableCompat.setTint(vectorDrawable, color)
            vectorDrawable.draw(canvas)
            return BitmapDescriptorFactory.fromBitmap(bitmap)
        }
    }

}