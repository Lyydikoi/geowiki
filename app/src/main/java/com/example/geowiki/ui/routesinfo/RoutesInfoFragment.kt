package com.example.geowiki.ui.routesinfo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.geowiki.base.BaseFragment
import com.example.geowiki.data.responsemodels.WikiRoute
import com.example.geowiki.databinding.RoutesInfoFragmentLayoutBinding
import com.example.geowiki.ui.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class RoutesInfoFragment : BaseFragment() {

    private val mapViewModel: MapViewModel by sharedViewModel()
    private lateinit var binding: RoutesInfoFragmentLayoutBinding
    private var adapter = RoutesRecyclerViewAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = RoutesInfoFragmentLayoutBinding.inflate(inflater, container, false)
        binding.layoutRoutesInfo.visibility = GONE
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setLiveDataListeners()
        initUI()
    }

    private fun initUI() {
        val linearSnapHelper = LinearSnapHelper()
        val linearLayoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        binding.rwRoutes.layoutManager = linearLayoutManager
        binding.rwRoutes.adapter = adapter
        linearSnapHelper.attachToRecyclerView(binding.rwRoutes)
    }

    private fun setLiveDataListeners() {
        mapViewModel.routesSuggestions.observe(viewLifecycleOwner, Observer { showRoutesInfo(it) })
        mapViewModel.routesFragmentState.observe(viewLifecycleOwner, Observer { showFragmentLayout(it) })
    }

    private fun showFragmentLayout(value: RoutesFragmentState) {
        binding.layoutRoutesInfo.visibility = if(value == RoutesFragmentState.OPENED) VISIBLE else GONE
        if (value == RoutesFragmentState.CLOSED) binding.rwRoutes.removeAllViewsInLayout()
    }

    private fun showRoutesInfo(routes: List<WikiRoute>?) {
        routes?.let {
            adapter.items = it
        }
    }

    companion object {
        const val TAG = "RoutesInfoFragment"

        @JvmStatic
        fun newInstance() = RoutesInfoFragment()
    }

}