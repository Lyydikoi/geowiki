package com.example.geowiki.ui.images

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.image_swipe_item_layout.view.*

class ImageSwipeCardViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val iv: ImageView = view.iv_wiki_image
}