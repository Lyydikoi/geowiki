package com.example.geowiki.ui
import android.content.Context
import android.location.Location
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.example.geowiki.R
import com.example.geowiki.data.responsemodels.Geosearch
import com.example.geowiki.databinding.MapFragmentBinding
import com.example.geowiki.utils.MAP_ZOOM_LEVEL_DEFAULT
import com.example.geowiki.utils.MapUtils.Companion.convertDpToPx
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnCameraMoveStartedListener.*
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import androidx.core.content.res.ResourcesCompat
import android.graphics.Color
import com.example.geowiki.data.responsemodels.WikiRoute
import com.example.geowiki.utils.MapUtils.Companion.addMarkerWithVectorIcon
import com.example.geowiki.utils.MapUtils.Companion.animateMapToLatLng
import com.example.geowiki.utils.MapUtils.Companion.getLatLngFromWikiLocation
import com.example.geowiki.utils.MapUtils.Companion.getPolylineFromWikiRoutes
import com.example.geowiki.utils.MapUtils.Companion.moveMapToLatLng
import com.example.geowiki.utils.MapUtils.Companion.moveMapToPolylineBounds
import com.example.geowiki.utils.MapUtils.Companion.setMapPadding


class MapFragment : Fragment(), OnMapReadyCallback {

    private val mapViewModel: MapViewModel by sharedViewModel()
    private lateinit var mapState: MapState
    private lateinit var cameraState: CameraState
    private lateinit var binding: MapFragmentBinding
    private lateinit var mMap: GoogleMap
    private val zoomLevel: Float = MAP_ZOOM_LEVEL_DEFAULT
    private var userLocationMarker: Marker? = null
    private var currentUserLocation: Location? = null
    private val onCameraIdleListener = GoogleMap.OnCameraIdleListener {
        mMap.cameraPosition.target?.let {
            if (cameraState == CameraState.GESTURE && mapState == MapState.MANUAL) {
                mapViewModel.fetchGeosearchResults("${it.latitude}|${it.longitude}")
            }
        }
    }
    private val onCameraMoveStartedListener = GoogleMap.OnCameraMoveStartedListener {
        when (it) {
            REASON_GESTURE -> {
                if (mapState != MapState.ROUTING) {
                    setMapManual()
                }
                setCameraState(CameraState.GESTURE)
                setBottomSheetState(BottomSheetState.HIDDEN)
            }
            REASON_API_ANIMATION -> setCameraState(CameraState.PROGRAMMATIC_MOVE)
            REASON_DEVELOPER_ANIMATION -> setCameraState(CameraState.DEVELOPER_MOVE)
        }
    }

    private val onMarkerClickListener = GoogleMap.OnMarkerClickListener { marker ->
        setMapManual()
        return@OnMarkerClickListener try {
            mapViewModel.setSelectedMarkerPosition(marker.position)
            animateMapToLatLng(marker.position, mMap, zoomLevel)
            fetchWikiArticleToShow(marker.tag as Int, getWikiArticleLocationParam(marker))
            true
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = MapFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map_fragment_map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        setGoogleMapListeners()
        setLiveDataListeners()
    }

    private fun setLiveDataListeners() {
        mapViewModel.geoSearch.observe(viewLifecycleOwner, Observer<Geosearch> { updateMarkers(it) })
        mapViewModel.mapState.observe(viewLifecycleOwner, Observer<MapState> { onMapStateChanged(it) })
        mapViewModel.cameraState.observe(viewLifecycleOwner, Observer<CameraState> { cameraState = it })
        mapViewModel.bottomSheetState.observe(viewLifecycleOwner, Observer {})
        mapViewModel.userLocation.observe(viewLifecycleOwner, Observer<Location>{
            currentUserLocation = it
            moveMapToUserLocation(it)
            moveUserLocationMarker(it)
        })
        mapViewModel.loadingSpinner.observe(viewLifecycleOwner, Observer<Boolean> {
            binding.progressBar.visibility = if(it) VISIBLE else GONE
        })
        mapViewModel.mapOffset.observe(viewLifecycleOwner, Observer<Float> { setBottomSheetMapOffset(it, context, mMap) })
        mapViewModel.routesSuggestions.observe(viewLifecycleOwner, Observer { drawRoutesSuggestions(it) })
    }

    private fun setGoogleMapListeners() {
        mMap.setOnCameraIdleListener(onCameraIdleListener)
        mMap.setOnCameraMoveStartedListener(onCameraMoveStartedListener)
        mMap.setOnMarkerClickListener(onMarkerClickListener)
        mMap.setOnCameraMoveListener {}
    }

    private fun fetchWikiArticleToShow(index: Int, location: String) {
        mapViewModel.getWikiLocationByIndex(index, location)?.let { wikiLocation ->
            wikiLocation.pageid?.let { pageId ->
                mapViewModel.fetchWikiArticle(pageId.toString())
            }
        }
    }

    private fun getWikiArticleLocationParam(marker: Marker): String {
        return "${marker.position.latitude}${marker.position.longitude}"
    }

    // TODO: refactor this
    private fun drawRoutesSuggestions(routes: List<WikiRoute>?) {
        var polyline = getPolylineFromWikiRoutes(routes)
        polyline?.let {
            val polyOptions: PolylineOptions = PolylineOptions()
                .width(10f)
                .jointType(JointType.ROUND)
                .color(Color.BLUE)
                .geodesic(true)
            polyOptions.addAll(polyline)
            mMap.clear()
            context?.let {
                mMap.setPadding(0, 0, 0, (Math.round(convertDpToPx(it, 300F))))
            }
            addUserLocationMarker(polyline.first())
            addWikiMarker(polyline.last())
            moveMapToPolylineBounds(polyline, mMap)
            mMap.addPolyline(polyOptions)
        }

    }

    private fun onMapStateChanged(it: MapState) {
        mapState = it
        when (mapState) {
            MapState.ROUTING -> { val padding = setMapPadding(300f, context, mMap) }
            MapState.MANUAL -> { val padding =setMapPadding(0f, context, mMap) }
            MapState.FOLLOWING -> { val padding =setMapPadding(0f, context, mMap) }
        }
    }

    // TODO: refactor this
    private fun setBottomSheetMapOffset(offset: Float?, context: Context?, map: GoogleMap) {
        offset?.let {
            var x = 1 + offset
            var bottomSheetMaxHeight = 0
            context?.let {
                if (x > 0 && x <= 1) {
                    // From hidden to collapsed state
                    bottomSheetMaxHeight = 300
                } else if (x > 1) {
                    // From collapsed to expanded state
                    bottomSheetMaxHeight = 300
                }
                var pxOffset = (Math.round(convertDpToPx(it, x * bottomSheetMaxHeight)))
                map.setPadding(0, 0, 0, pxOffset)
            }
        }
    }

    private fun updateMarkers(resultsList: Geosearch?) {
        resultsList?.let { result ->
            mMap.clear()
            currentUserLocation?.let { addUserLocationMarker(LatLng(it.latitude, it.longitude)) }
            result.wikiLocationsList?.let {
                it.forEachIndexed { index, wikiLocation ->
                    val position = getLatLngFromWikiLocation(wikiLocation)
                    position?.let {
                        addWikiMarker(it, index)
                    }
                }
            }
        }
    }

    private fun addWikiMarker(position: LatLng, index: Int = Int.MAX_VALUE) {
        val vectorDrawable = ResourcesCompat.getDrawable(resources, R.drawable.ic_location_48dp, null)
        vectorDrawable?.let {
            addMarkerWithVectorIcon(mMap, vectorDrawable, position, Color.parseColor("#6C33A8"),  index)
        }
    }

    private fun addUserLocationMarker(position: LatLng) {
        val vectorDrawable = ResourcesCompat.getDrawable(resources, R.drawable.ic_navigation_36dp, null)
        vectorDrawable?.let {
            userLocationMarker = addMarkerWithVectorIcon(mMap, vectorDrawable, position, Color.parseColor("#2FA89A"),  0)
        }
    }

    private fun moveMapToUserLocation(location: Location, forceMove: Boolean = false) {
        if (mapState == MapState.FOLLOWING || forceMove) {
            moveMapToLatLng(LatLng(location.latitude, location.longitude), mMap, zoomLevel)
        }
    }

    private fun moveUserLocationMarker(location: Location) {
        userLocationMarker?.let {
            it.setPosition(LatLng(location.latitude, location.longitude))
        } ?: addUserLocationMarker(LatLng(location.latitude, location.longitude))
    }

    private fun setMapManual() {
        mapViewModel.setMapState(MapState.MANUAL)
    }

    private fun setCameraState(value: CameraState) {
        mapViewModel.setCameraState(value)
    }

    private fun setBottomSheetState(state: BottomSheetState) {
        mapViewModel.setBottomSheetState(state)
    }

}
