package com.example.geowiki.ui

import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.view.View
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil.setContentView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.geowiki.R
import com.example.geowiki.base.BaseActivity
import com.example.geowiki.data.responsemodels.Article
import com.example.geowiki.databinding.ActivityMainBinding
import com.example.geowiki.ui.images.ImageSwipeRecyclerViewAdapter
import com.example.geowiki.ui.routesinfo.RoutesInfoFragment
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import org.koin.androidx.viewmodel.ext.android.viewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior


class MainActivity : BaseActivity(), View.OnClickListener {

    private val binding: ActivityMainBinding by lazy {
        setContentView(this, R.layout.activity_main) as ActivityMainBinding
    }
    private val mapViewModel: MapViewModel by viewModel()

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>
    private lateinit var routesFragmentState: RoutesFragmentState
    private lateinit var bottomSheetState: BottomSheetState
    private lateinit var currentUserLocation: Location
    private lateinit var selectedMarkerPosition: LatLng

    private var requestedArticleId = ""
    private var isFirstLocationUpdate = false
    private var adapter = ImageSwipeRecyclerViewAdapter()

    private val linearSnapHelper = LinearSnapHelper()
    private val linearLayoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            super.onLocationResult(locationResult)

            locationResult?.let {
                it.locations[0]?.let { location ->
                    if (isFirstLocationUpdate) {
                        // If it is first time, when user location was found fetch data as well
                        mapViewModel.setIsFirstLocationUpdate(false)
                        mapViewModel.fetchGeosearchResults("${location.latitude}|${location.longitude}")
                    }

                    updateUserLocation(location)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.lifecycleOwner = this
        attachUiFragments()
        initView()
        requestLocation()
        setLiveDataListeners()
    }

    @SuppressLint("MissingPermission")
    override fun initLocationUpdates() {
        fusedLocationClient.lastLocation.addOnSuccessListener {
            it?.let { location ->
                updateUserLocation(location)
                mapViewModel.fetchGeosearchResults("${location.latitude}|${location.longitude}")
            }

            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())
        }
    }

    private fun initView() {
        bottomSheetBehavior = BottomSheetBehavior.from<ConstraintLayout>(binding.bsMapInfo.layoutBottomSheet)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                mapViewModel.setMapOffset(slideOffset)
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        bottomSheetState = BottomSheetState.COLLAPSED
                    }
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        bottomSheetState = BottomSheetState.HIDDEN
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        bottomSheetState = BottomSheetState.EXPANDED
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }
            }
        })

        binding.bsMapInfo.rwBottomBar.layoutManager = linearLayoutManager
        binding.bsMapInfo.rwBottomBar.adapter = adapter
        binding.bsMapInfo.tvBottomSheetDirections.setOnClickListener(this)
        linearSnapHelper.attachToRecyclerView(binding.bsMapInfo.rwBottomBar)
    }

    private fun attachUiFragments() {
        addMapFeatureFragment(RoutesInfoFragment.newInstance(), RoutesInfoFragment.TAG)
    }

    private fun setLiveDataListeners() {
        mapViewModel.wikiArticle?.let {
            it.observe(this, Observer<Article> { article ->
                article?.let { data ->
                    showWikiLocationInfo(data)
                }
            })
        }
        mapViewModel.requestedPageId.observe(this, Observer { requestedArticleId = it })
        mapViewModel.isFirstLcationUpdate.observe(this, Observer { isFirstLocationUpdate = it })
        mapViewModel.userLocation.observe(this, Observer { currentUserLocation = it })
        mapViewModel.selectedMarkerPosition.observe(this, Observer { selectedMarkerPosition = it })
        mapViewModel.routesFragmentState.observe(this, Observer { routesFragmentState = it })
        mapViewModel.bottomSheetState.observe(this, Observer {
            when (it) {
                BottomSheetState.EXPANDED -> bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                BottomSheetState.COLLAPSED -> bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                BottomSheetState.HIDDEN -> bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            }
        })
    }

    override fun onClick(view: View?) {
        view?.let {
            if (view.id == binding.bsMapInfo.tvBottomSheetDirections.id) {
                mapViewModel.setBottomSheetState(BottomSheetState.HIDDEN)
                mapViewModel.setMapState(MapState.ROUTING)
                mapViewModel.setRoutesFragmentState(RoutesFragmentState.OPENED)
                fetchRoutesInfo()
            }
        }
    }

    private fun showWikiLocationInfo(article: Article) {
        if (requestedArticleId == article.pageid.toString()) {
            mapViewModel.setRoutesFragmentState(RoutesFragmentState.CLOSED)
            mapViewModel.setBottomSheetState(BottomSheetState.COLLAPSED)
            binding.bsMapInfo.tvBottomSheetHeader.text = article.title
            binding.bsMapInfo.tvBottomSheetText.text = article.description
            binding.bsMapInfo.tvBottomSheetLink.text = article.fullurl
            article.images?.let { adapter.items = it }
        }
    }

    private fun updateUserLocation(location: Location) {
        mapViewModel.setUserLocation(location)
    }

    private fun fetchRoutesInfo() {
        try {

            mapViewModel.fetchRoutes(
                currentUserLocation.latitude.toString(), currentUserLocation.longitude.toString(),
                selectedMarkerPosition.latitude.toString(), selectedMarkerPosition.longitude.toString()
            )

        } catch (e: Exception) {
            Toast.makeText(this, getString(R.string.routes_suggestion_error), Toast.LENGTH_SHORT)
        }
    }

    override fun onDestroy() {
        mapViewModel.setBottomSheetState(BottomSheetState.HIDDEN)
        mapViewModel.setRoutesFragmentState(RoutesFragmentState.CLOSED)
        super.onDestroy()
    }

    override fun onBackPressed() {
        if (routesFragmentState == RoutesFragmentState.OPENED) {

            mapViewModel.setMapState(MapState.MANUAL)
            mapViewModel.setRoutesFragmentState(RoutesFragmentState.CLOSED)
            currentUserLocation?.let {
                mapViewModel.fetchGeosearchResults("${currentUserLocation.latitude}|${currentUserLocation.longitude}")
            }

        } else if (bottomSheetState == BottomSheetState.EXPANDED || bottomSheetState == BottomSheetState.COLLAPSED) {

            mapViewModel.setBottomSheetState(BottomSheetState.HIDDEN)

        } else {
            super.onBackPressed()
        }
    }

}
