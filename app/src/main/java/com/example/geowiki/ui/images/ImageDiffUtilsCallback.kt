package com.example.geowiki.ui.images

import androidx.recyclerview.widget.DiffUtil
import com.example.geowiki.data.responsemodels.Image

class ImageDiffUtilCallback(private val oldList: List<Image>, private val newList: List<Image>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].title == newList[newItemPosition].title
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }
}