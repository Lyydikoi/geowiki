package com.example.geowiki.ui.routesinfo

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.routes_item_layout.view.*

class RoutesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val ivIcon: ImageView = view.iv_route_icon
    val tvDuration: TextView = view.tv_duration_value
    val tvDistance: TextView = view.tv_distance_value
    val tvMode: TextView = view.tv_route_mode
}