package com.example.geowiki.ui.routesinfo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.geowiki.R
import com.example.geowiki.data.responsemodels.WikiRoute
import kotlin.properties.Delegates

class RoutesRecyclerViewAdapter : RecyclerView.Adapter<RoutesViewHolder>() {

    var items: List<WikiRoute> by Delegates.observable(emptyList()) { _, oldList, newList ->
        notifyChanges(oldList, newList)
    }

    private fun notifyChanges(oldList: List<WikiRoute>, newList: List<WikiRoute>) {
        val diff = DiffUtil.calculateDiff(RoutesDiffUtilCallback(oldList, newList))
        diff.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoutesViewHolder {
        return RoutesViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.routes_item_layout, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RoutesViewHolder, position: Int) {
        items[position]?.let {
            holder.tvMode.text = it.mode
            holder.tvDistance.text = it.distance.toString()
            holder.tvDuration.text = it.duration.toString()
        }
    }

}