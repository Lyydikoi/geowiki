package com.example.geowiki.ui.images

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.geowiki.data.responsemodels.Image
import kotlin.properties.Delegates
import com.example.geowiki.R


class ImageSwipeRecyclerViewAdapter : RecyclerView.Adapter<ImageSwipeCardViewHolder>() {

    var items: List<Image> by Delegates.observable(emptyList()) { _, oldList, newList ->
        notifyChanges(oldList, newList)
    }

    private fun notifyChanges(oldList: List<Image>, newList: List<Image>) {
        val diff = DiffUtil.calculateDiff(ImageDiffUtilCallback(oldList, newList))
        diff.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ImageSwipeCardViewHolder {
        return ImageSwipeCardViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.image_swipe_item_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ImageSwipeCardViewHolder, position: Int) {
        items[position].title?.let {
            Glide.with(holder.itemView.context)
                .load("https://commons.wikimedia.org/wiki/Special:FilePath/$it?width=200")
                .placeholder(R.drawable.ic_image_black_56dp)
                .into(holder.iv)
        }
    }
}