package com.example.geowiki.ui.routesinfo

import androidx.recyclerview.widget.DiffUtil
import com.example.geowiki.data.responsemodels.WikiRoute

class RoutesDiffUtilCallback(private val oldList: List<WikiRoute>, private val newList: List<WikiRoute>) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].copy() == newList[newItemPosition].copy()
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }
}