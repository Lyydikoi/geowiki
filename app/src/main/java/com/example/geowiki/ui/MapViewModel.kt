package com.example.geowiki.ui

import android.location.Location
import androidx.lifecycle.*
import com.example.geowiki.data.repositories.*
import com.example.geowiki.data.responsemodels.*
import com.example.geowiki.network.RequestResult
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.launch
import java.lang.Exception

class MapViewModel(
    private val wikiRepository: WikiRepository,
    private val locationRepository: LocationRepository,
    private val routesRepository: RoutesRepo
) : ViewModel() {

    private val _isIFirstLocationUpdate = MutableLiveData<Boolean>()

    val isFirstLcationUpdate: LiveData<Boolean>
        get() = _isIFirstLocationUpdate

    val userLocation: LiveData<Location> = locationRepository.userLocation

    val selectedMarkerPosition: LiveData<LatLng> = locationRepository.selectedMarkerPosition

    private val _mapOffset = MutableLiveData<Float>()

    val mapOffset: LiveData<Float>
        get() = _mapOffset

    private val _mapState = MutableLiveData<MapState>()

    val mapState: LiveData<MapState>
        get() = _mapState

    private val _cameraState = MutableLiveData<CameraState>()

    val cameraState: LiveData<CameraState>
        get() = _cameraState

    private val _bottomSheetState = MutableLiveData<BottomSheetState>()

    val bottomSheetState: LiveData<BottomSheetState>
        get() = _bottomSheetState

    private val _routesFragmentState = MutableLiveData<RoutesFragmentState>()

    val routesFragmentState: LiveData<RoutesFragmentState>
        get() = _routesFragmentState

    private val _loadingSpinner = MutableLiveData<Boolean>()

    val loadingSpinner: LiveData<Boolean>
        get() = _loadingSpinner

    private val _snackBarMessage = MutableLiveData<String>()

    val snackbarMessage: LiveData<String>
        get() = _snackBarMessage

    private val _requestedPageId = MutableLiveData<String>()

    val requestedPageId: LiveData<String>
        get() = _requestedPageId

    private val _wikiArticleResponse = MutableLiveData<WikiPagesResponse>()

    val wikiArticle: LiveData<Article>? = Transformations.map(_wikiArticleResponse) {
        // TODO: refactor this: create helper
        var result: Article? = null
        try {
            it.pagesSet?.pages?.forEach { (key, value) ->
                result = value
            }
            result
        } catch (e: Exception) {
            e.printStackTrace()
            result
        }
    }

    private val _geoSearchResponse = MutableLiveData<GeosearchResponse>()

    val geoSearch: LiveData<Geosearch> = Transformations.map(_geoSearchResponse) {
        it.geosearch
    }

    private val _routesResponse = MutableLiveData<RoutesResponse>()

    val routesSuggestions: LiveData<List<WikiRoute>> = Transformations.map(_routesResponse) {
        // TODO: refactor this, create helper
        var result: List<WikiRoute>? = null
        try {
            it.data?.forEach { (key, value) ->
                value?.let {
                    it.itineraries?.get(0)?.let {
                        result = it.legs
                    }
                }
            }
            result
        } catch (e: Exception) {
            e.printStackTrace()
            result
        }
    }

    init {
        _isIFirstLocationUpdate.postValue(true)
        setMapState(MapState.FOLLOWING)
        setCameraState(CameraState.GESTURE)
    }

    fun fetchGeosearchResults(coord: String, radius: String = "10000") {
        launchDataLoad(_geoSearchResponse) { wikiRepository.fetchGeoSearchResults(coord, radius) }
    }

    fun fetchWikiArticle(pageid: String) {
        _requestedPageId.postValue(pageid)
        launchDataLoad(_wikiArticleResponse) { wikiRepository.fetchWikiArticle(pageid) }
    }

    fun fetchRoutes(startLat: String, startLng: String, destLat: String, destLng: String) {
        launchDataLoad(_routesResponse) { routesRepository.fetchRoutes(startLat, startLng, destLat, destLng)}
    }

    fun setUserLocation(location: Location) {
        locationRepository.setUserLocation(location)
    }

    fun setSelectedMarkerPosition(position: LatLng) {
        locationRepository.setSelectedMarkerPosition(position)
    }

    fun setMapState(state: MapState) {
        _mapState.postValue(state)
    }

    fun setCameraState(state: CameraState) {
        _cameraState.postValue(state)
    }

    fun setBottomSheetState(state: BottomSheetState) {
        _bottomSheetState.postValue(state)
    }

    fun setRoutesFragmentState(state: RoutesFragmentState) {
        _routesFragmentState.postValue(state)
    }

    fun setIsFirstLocationUpdate(value: Boolean) {
        _isIFirstLocationUpdate.postValue(value)
    }

    /**
     * When map marker clicked, we need to find WikiLocation info from latest data set.
     * To avoid mismatch, when WikiLocations updated while marker clicked,
     * compare coordinates of the marker with coordinates of the WikiLocation object.
     *
     * @param index of the WikiLocaion in data list fetched from server.
     * @param location string composed of marker coordinates.
     */
    fun getWikiLocationByIndex(index: Int, location: String): WikiLocation? {
        try {
            geoSearch.value?.let {
                it.wikiLocationsList?.let { wikiLocationsList ->
                    val result= wikiLocationsList[index]
                    return if ("${result.lat}${result.lon}" == location) {
                        result
                    } else {
                        null
                    }
                } ?: return null
            } ?: return null
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }
    }

    /**
     * Helper function to call data load function with a loading spinner and errors triggered in a snackbar.
     *
     * @param mld to which result will be set.
     * @param block lambda to actually load data.
     */
    private fun <T> launchDataLoad(mld: MutableLiveData<T>, block: suspend () -> RequestResult<T>) {

        viewModelScope.launch {
            try {
                _loadingSpinner.value = true
                when (val response = block()) {
                    is RequestResult.Success -> mld.postValue(response.data)
                    is RequestResult.Error -> _snackBarMessage.postValue(response.message)
                }
            } catch (e: Exception) {
                e.message?.let { _snackBarMessage.value = it }
            } finally {
                _loadingSpinner.value = false
            }
        }
    }

    fun setMapOffset(slideOffset: Float) {
        _mapOffset.postValue(slideOffset)
    }
}

enum class MapState {
    MANUAL,
    FOLLOWING,
    ROUTING
}

enum class CameraState {
    GESTURE,
    PROGRAMMATIC_MOVE,
    DEVELOPER_MOVE
}

enum class BottomSheetState {
    EXPANDED,
    COLLAPSED,
    HIDDEN
}

enum class RoutesFragmentState {
    OPENED,
    CLOSED
}
