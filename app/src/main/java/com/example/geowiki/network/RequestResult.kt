package com.example.geowiki.network

/**
 * A generic class that holds a value with its loading status.
 */
sealed class RequestResult<out R> {

    data class Success<out T>(val data: T) : RequestResult<T>()
    data class Error(val message: String) : RequestResult<Nothing>()
    object Loading : RequestResult<Nothing>()

    override fun toString() : String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[message=$message]"
            Loading -> "Loading"
        }
    }
}

/**
 * `true` if [RequestResult] is of type [Success] & holds non-null [Success.data].
 */
val RequestResult<*>.successed
    get() = this is RequestResult.Success && data != null