package com.example.geowiki.network

import com.example.geowiki.data.responsemodels.RoutesResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface TransitApi {

    @POST("graphql")
    suspend fun getRoutes(@Body body: GraphqlRequestBody): Response<RoutesResponse>
}