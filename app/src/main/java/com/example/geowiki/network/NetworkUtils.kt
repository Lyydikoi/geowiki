package com.example.geowiki.network

import android.os.Parcel
import android.os.Parcelable
import com.example.geowiki.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

// Utils for Wiki Api Retrofit client
fun provideWikiRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

fun provideWikiOkHttpClient(): OkHttpClient {
    val interceptor : HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        this.level = HttpLoggingInterceptor.Level.BODY
    }

    return OkHttpClient().newBuilder()
        .readTimeout(25, TimeUnit.SECONDS)
        .connectTimeout(25, TimeUnit.SECONDS)
        //.addInterceptor(interceptor)
        .build()
}

fun provideGeowikiApi(retrofit: Retrofit): GeowikiApi = retrofit.create(GeowikiApi::class.java)


// Utils for Digitransit Retrofit client
fun provideTransitRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL_DIGITRANSIT)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

fun provideTransitOkHttpClient(): OkHttpClient {
    val interceptor : HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        this.level = HttpLoggingInterceptor.Level.BODY
    }

    return OkHttpClient().newBuilder()
        .readTimeout(25, TimeUnit.SECONDS)
        .connectTimeout(25, TimeUnit.SECONDS)
        .addInterceptor(interceptor)
        .build()
}

fun provideDigitransitApi(retrofit: Retrofit): TransitApi = retrofit.create(TransitApi::class.java)

// Other network related things
data class GraphqlRequestBody(
    var query: String
)

fun getRoutesGraphqlQuery(startLat: String, startLng: String, destLat: String, destLng: String): GraphqlRequestBody {
    return GraphqlRequestBody(
       "{ plan( " +
                "from: {lat: $startLat, lon: $startLng} " +
                "to: {lat: $destLat, lon: $destLng} " +
                "numItineraries: 1) " +
                "{ itineraries " +
                "{ legs " +
                "{ startTime endTime mode duration realTime distance transitLeg legGeometry " +
                "{ points } } } } }"
    )
}