package com.example.geowiki.network

import com.example.geowiki.data.responsemodels.GeosearchResponse
import com.example.geowiki.data.responsemodels.RoutesResponse
import com.example.geowiki.data.responsemodels.WikiPagesResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface GeowikiApi {

    @GET("api.php")
    suspend fun getGeoearchResults(
        @Query("action") action: String,
        @Query("list") list: String,
        @Query("gsradius") gsradius: String,
        @Query("gscoord") gscoord: String,
        @Query("gslimit") gslimit: String,
        @Query("format") format: String
    ): Response<GeosearchResponse>

    @GET("api.php")
    suspend fun getWikiPageResults(
        @Query("action") action: String,
        @Query("prop") prop: String,
        @Query("pageids") pagesid: String,
        @Query("format") format: String,
        @Query("inprop") inprop: String
    ): Response<WikiPagesResponse>

}