package com.example.geowiki.data.source.remote

import com.example.geowiki.network.RequestResult
import com.example.geowiki.data.responsemodels.GeosearchResponse
import com.example.geowiki.data.responsemodels.WikiPagesResponse
import com.example.geowiki.network.GeowikiApi

class WikiRemoteDataSource(
    private val service: GeowikiApi
) : BaseDataSource() {

    suspend fun fetchGeosearchResults(coord: String, radius: String): RequestResult<GeosearchResponse>
            = getResult { service.getGeoearchResults("query", "geosearch", radius, coord, "50", "json") }

    suspend fun fetchWikiArticle(pagesid: String): RequestResult<WikiPagesResponse>
            = getResult { service.getWikiPageResults("query", "info|description|images", pagesid, "json", "url") }

}

