package com.example.geowiki.data.repositories

import com.example.geowiki.data.responsemodels.RoutesResponse
import com.example.geowiki.data.source.remote.RoutesRemoteDataSource
import com.example.geowiki.network.RequestResult
import com.example.geowiki.network.getRoutesGraphqlQuery
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RoutesRepoImpl(
    private val remoteDataSource: RoutesRemoteDataSource
): RoutesRepo {

    override suspend fun fetchRoutes(
        startLat: String, startLng: String, destLat: String, destLng: String
    ): RequestResult<RoutesResponse> {

        return withContext(Dispatchers.IO) {
            remoteDataSource.fetchRoutes(getRoutesGraphqlQuery(startLat, startLng, destLat, destLng))
        }

    }

}