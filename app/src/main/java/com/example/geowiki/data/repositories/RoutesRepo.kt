package com.example.geowiki.data.repositories

import com.example.geowiki.data.responsemodels.RoutesResponse
import com.example.geowiki.network.RequestResult

interface RoutesRepo {

    suspend fun fetchRoutes(
        startLat: String, startLng: String, destLat: String, destLng: String
    ): RequestResult<RoutesResponse>

}