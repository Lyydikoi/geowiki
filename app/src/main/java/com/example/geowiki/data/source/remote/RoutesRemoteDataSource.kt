package com.example.geowiki.data.source.remote

import com.example.geowiki.data.responsemodels.RoutesResponse
import com.example.geowiki.network.GraphqlRequestBody
import com.example.geowiki.network.RequestResult
import com.example.geowiki.network.TransitApi

class RoutesRemoteDataSource(
    private val service: TransitApi
) : BaseDataSource() {

    suspend fun fetchRoutes(graphQl: GraphqlRequestBody): RequestResult<RoutesResponse>
            = getResult { service.getRoutes(graphQl) }

}