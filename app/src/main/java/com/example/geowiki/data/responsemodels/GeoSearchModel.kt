package com.example.geowiki.data.responsemodels

import com.google.gson.annotations.SerializedName

data class GeosearchResponse(
    @SerializedName("query")
    val geosearch: Geosearch?
)

data class Geosearch (
    @SerializedName("geosearch")
    val wikiLocationsList: List<WikiLocation>?
)

data class WikiLocation(
    @SerializedName("pageid")
    val pageid: Int?,
    @SerializedName("ns")
    val ns: Int?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("lat")
    val lat: Double?,
    @SerializedName("lon")
    val lon: Double?,
    @SerializedName("dist")
    val dist: Float?,
    @SerializedName("primary")
    val primary: String?
)