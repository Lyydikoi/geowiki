package com.example.geowiki.data.responsemodels
import com.google.gson.annotations.SerializedName


data class RoutesResponse(
    @SerializedName("data")
    val data: HashMap<String, Plan>?
)

data class Plan(
    @SerializedName("itineraries")
    val itineraries: List<Itinerary>?
)

data class Itinerary(
    @SerializedName("legs")
    val legs: List<WikiRoute>?
)

data class WikiRoute(
    @SerializedName("startTime")
    val startTime: Long?,
    @SerializedName("endTime")
    val endTime: Long?,
    @SerializedName("mode")
    val mode: String?,
    @SerializedName("duration")
    val duration: Long?,
    @SerializedName("realTime")
    val realTime: Boolean?,
    @SerializedName("distance")
    val distance: Float?,
    @SerializedName("transitLeg")
    val transitLeg: Boolean?,
    @SerializedName("legGeometry")
    val legGeometry: HashMap<String, String>?
)

