package com.example.geowiki.data.repositories

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng

class LocationRepository {
    private val _userLocation = MutableLiveData<Location>()

    val userLocation: LiveData<Location>
        get() = _userLocation

    private val _selectedMarkerPosition = MutableLiveData<LatLng>()

    val selectedMarkerPosition: LiveData<LatLng>
        get() = _selectedMarkerPosition

    fun setSelectedMarkerPosition(position: LatLng) {
        _selectedMarkerPosition.postValue(position)
    }

    fun setUserLocation(location: Location) {
        _userLocation.postValue(location)
    }


}