package com.example.geowiki.data.repositories

import com.example.geowiki.data.responsemodels.GeosearchResponse
import com.example.geowiki.data.responsemodels.WikiPagesResponse
import com.example.geowiki.network.RequestResult

interface WikiRepository {

    suspend fun fetchGeoSearchResults(coord: String, radius: String): RequestResult<GeosearchResponse>

    suspend fun fetchWikiArticle(pageids: String): RequestResult<WikiPagesResponse>

}