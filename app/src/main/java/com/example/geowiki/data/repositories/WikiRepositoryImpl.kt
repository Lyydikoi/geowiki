package com.example.geowiki.data.repositories

import com.example.geowiki.data.responsemodels.GeosearchResponse
import com.example.geowiki.data.responsemodels.WikiPagesResponse
import com.example.geowiki.data.source.remote.WikiRemoteDataSource
import com.example.geowiki.network.RequestResult
import kotlinx.coroutines.*

class WikiRepositoryImpl(
    private val remoteDataSource: WikiRemoteDataSource
) : WikiRepository {

    override suspend fun fetchGeoSearchResults(coord: String, radius: String): RequestResult<GeosearchResponse> {
        return withContext(Dispatchers.IO) {
            remoteDataSource.fetchGeosearchResults(coord, radius)
        }
    }

    override suspend fun fetchWikiArticle(pageids: String): RequestResult<WikiPagesResponse> {
        return withContext(Dispatchers.IO) {
            remoteDataSource.fetchWikiArticle(pageids)
        }
    }

}