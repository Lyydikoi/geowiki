package com.example.geowiki.base

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.geowiki.R
import com.example.geowiki.utils.LOCATION_REQUEST_INTERVAL
import com.example.geowiki.utils.REQUEST_CHECK_SETTINGS
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task

abstract class BaseActivity : AppCompatActivity() {

    protected val locationRequest by lazy { LocationRequest() }
    protected val fusedLocationClient: FusedLocationProviderClient by lazy {
        LocationServices.getFusedLocationProviderClient(this)
    }

    fun addMapFeatureFragment(baseFragment: BaseFragment, tag: String) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.root_layout, baseFragment, tag)
        fragmentTransaction.setCustomAnimations(
            R.animator.enter_from_bottom,
            R.animator.exit_to_bottom,
            R.animator.enter_from_bottom,
            R.animator.exit_to_bottom)
        fragmentTransaction.commit()
    }

    // All locations related thing go below.
    @SuppressLint("MissingPermission")
    abstract fun initLocationUpdates()

    protected fun requestLocation() {
        if (locationPermissionGratned()) {
            setUpLocationRequest()
            getLocationSettings()?.let {
                it.addOnSuccessListener {
                    // All location settings are satisfied.
                    initLocationUpdates()
                }
                it.addOnFailureListener { exception ->
                    if (exception is ResolvableApiException){
                        // Location settings are not satisfied, but this can be fixed by showing the user a dialog.
                        try {
                            exception.startResolutionForResult(this@BaseActivity, REQUEST_CHECK_SETTINGS)
                        } catch (sendEx: IntentSender.SendIntentException) {
                            sendEx.printStackTrace()
                        }
                    }
                }
            }
        }
    }

    private fun locationPermissionGratned(): Boolean {
        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 9)
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 9) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestLocation()
            }
        }
    }

    private fun setUpLocationRequest() {
        locationRequest.interval = LOCATION_REQUEST_INTERVAL
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun getLocationSettings(): Task<LocationSettingsResponse>? {
        val settingsBuilder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        val settingsClient: SettingsClient = LocationServices.getSettingsClient(this)
        return  settingsClient.checkLocationSettings(settingsBuilder.build())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CHECK_SETTINGS && resultCode == Activity.RESULT_OK) {
            requestLocation()
        }
    }

}