package com.example.geowiki.base

import android.app.Application
import com.example.geowiki.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class GeowikiApp : Application() {
    override fun onCreate() {
        super.onCreate()

        // Start Koin DI
        startKoin{
            androidLogger()
            androidContext(this@GeowikiApp)
            modules(appModule)
        }
    }

}